/* bytectl.h
 * Header-only C library providing an alternative to structs
 * that offers finer control over byte layout
 * at the cost of alignment guarantees and value-copying semantics.
 * LITTLE-ENDIAN ARCHITECTURE REQUIRED.
 * Also note that due to the use of a 16-bit offset bitfield,
 * objects' effective maximum size is 64KiB. */
#pragma once
#include <stdint.h>
#include <stdlib.h>

#ifdef UINT64_MAX
typedef uint64_t uintbig;
#else
typedef uint32_t uintbig;
#endif

/* These functions each take at least
 * a void pointer argument called object
 * and a uint32_t argument called field.
 * Field identifies which "struct" field to access in the object.
 * I recommend defining field identifiers in an enum.
 * The 16 msbs of field are the field's byte offset into object.
 * The next 8 msbs are the field's size in bytes.
 * Size in bytes is capped at sizeof(uintbig) for bcget and bcset,
 * but may exceed it for bcload and bcstore.
 * The 8 lsbs of the field identifier are the field's bitmask.
 * If bitmask is not 0xff, it is regarded as in use,
 * and so size will be treated as 1, as 1 byte is the only integer size
 * to which a 1-byte bitmask can be meaningfully and efficiently applied.
 * Bitmask 0xff is regarded as indicating no bitmask is to be used,
 * thus allowing for larger field sizes.
 * If preprocessor symbol BYTECTL_DONT_SHIFT_BITFIELDS is defined,
 * bits extracted via bitmask are left in place;
 * if it is not defined, such bits are shifted as appropriate
 * to seamlessly emulate an n-bit sub-byte integer
 * from the caller's perspective. */

/* bcget: Fetches the value of a field from an object. */
static inline uintbig
bcget(
    const void *object,
    uint32_t field
) {
    const uint8_t *data = object;
    uint16_t offset = field >> 16;
    uint8_t size = field >> 8;
    uint8_t bitmask = field;
    uintbig value;
    switch (bitmask) {
        case 0: {
            value = 0;
        } break;
        case 0xff: {
            switch (size) {
                case 0: {
                    value = 0;
                } break;
                case 1: {
                    value = data[offset];
                } break;
                case 2: {
                    value = *(const uint16_t *) &data[offset];
                } break;
                case 3: {
                    value = (*(const uint32_t *) &data[offset])
                        & 0x00ffffff;
                } break;
                case 4: {
                    value = *(const uint32_t *) &data[offset];
                } break;
#ifdef UINT64_MAX
                case 5: {
                    value = (*(const uint64_t *) &data[offset])
                        & 0x000000ffffffffff;
                } break;
                case 6: {
                    value = (*(const uint64_t *) &data[offset])
                        & 0x0000ffffffffffff;
                } break;
                case 7: {
                    value = (*(const uint64_t *) &data[offset])
                        & 0x00ffffffffffffff;
                } break;
#endif
                default: {
                    value = *(const uintbig *) &data[offset];
                }
            }
        } break;
        default: {
#ifdef BYTECTL_DONT_SHIFT_BITFIELDS
            value = data[offset] & bitmask;
#else
            value = (data[offset] & bitmask) >> __builtin_ctz(bitmask);
        }
#endif
    }
    return value;
}

/* bcset: Sets the value of a field in an object. */
static inline uintbig
bcset(
    void *object,
    uint32_t field,
    uintbig value
) {
    uint8_t *data = object;
    uint16_t offset = field >> 16;
    uint8_t size = field >> 8;
    uint8_t bitmask = field;
    switch (bitmask) {
        case 0: {} break;
        case 0xff: {
            switch (size) {
                case 0: {} break;
                case 1: {
                    value &= 0xff;
                    data[offset] = value;
                } break;
                case 2: {
                    value &= 0xffff;
                    *((uint16_t *) &data[offset]) = value;
                } break;
                case 3: {
                    value &= 0x00ffffff;
                    *((uint32_t *) &data[offset]) &= 0xff000000;
                    *((uint32_t *) &data[offset]) |= value;
                } break;
                case 4: {
#ifdef UINT64_MAX
                    value &= 0xffffffff;
#endif
                    *((uint32_t *) &data[offset]) = value;
                } break;
#ifdef UINT64_MAX
                case 5: {
                    value &= 0x000000ffffffffff;
                    *((uint64_t *) &data[offset]) &= 0xffffff0000000000;
                    *((uint64_t *) &data[offset]) |= value;
                } break;
                case 6: {
                    value &= 0x0000ffffffffffff;
                    *((uint64_t *) &data[offset]) &= 0xffff000000000000;
                    *((uint64_t *) &data[offset]) |= value;
                } break;
                case 7: {
                    value &= 0x00ffffffffffffff;
                    *((uint64_t *) &data[offset]) &= 0xff00000000000000;
                    *((uint64_t *) &data[offset]) |= value;
                } break;
#endif
                default: {
                    *((uintbig *) &data[offset]) = value;
                } break;
            }
        } break;
        default: {
            data[offset] &= ~bitmask;
#ifdef BYTECTL_DONT_SHIFT_BITFIELDS
            value &= bitmask;
#else
            value = (value << __builtin_ctz(bitmask)) & bitmask;
#endif
            data[offset] |= value;
        }
    }
    return value;
}

/* bcaddr: Calculates a non-const pointer to a field in an object. */
static inline void
*bcaddr(
    void *object,
    uint32_t field
) {
    uint8_t *data = object;
    uint16_t offset = field >> 16;
    return &data[offset];
}

/* bccaddr: Calculates a const pointer to a field in an object. */
static inline const void
*bccaddr(
    const void *object,
    uint32_t field
) {
    const uint8_t *data = object;
    uint16_t offset = field >> 16;
    return &data[offset];
}

/* bcload: Like bcget, except that data extracted is written by pointer,
 * and the field is accordingly allowed to exceed sizeof(uintbig) bytes.
 * The address passed as "to" is written to and returned.
 * Bitmask is never respected even if size is 1. */
static inline void
*bcload(
    const void *object,
    uint32_t field,
    void *to
) {
    const uint8_t *data = object;
    uint16_t offset = field >> 16;
    uint8_t size = field >> 8;
    memcpy(to, &data[offset], size);
    return to;
}

/* bcstore: Like bcset, except that data to write is read by pointer,
 * and the field is accordingly allowed to exceed sizeof(uintbig) bytes.
 * The address passed as "from" is read from,
 * and the address written to is returned.
 * Bitmask is never respected even if size is 1. */
static inline void
*bcload(
    void *object,
    uint32_t field,
    const void *from
) {
    uint8_t *data = object;
    uint16_t offset = field >> 16;
    uint8_t size = field >> 8;
    memcpy(&data[offset], from, size);
    return &data[offset];
}

/* These constants allow for reflective inspection
 * of object field identifiers as objects themselves.
 * You can also use them to construct field identifiers
 * in a more self-documenting manner, if perhaps more verbose,
 * but for performance reasons I do not recommend this,
 * as it mandates that the field identifiers be A) computed at runtime
 * and B) loaded from runtime storage every time they must be applied.
 * The test suite takes this approach, but only for testing purposes.
 * On the other hand, these constants do facilitate
 * runtime construction of object field identifiers
 * in the case that doing so is absolutely necessary,
 * e.g. if the object layout depends on runtime information,
 * so there is that. */
enum {
    bc_field_bitmask        = 0x000001ff,
    bc_field_size           = 0x000101ff,
    bc_field_offset         = 0x000202ff
};
