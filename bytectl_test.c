#include <stdio.h>
#include "bytectl.h"

int main() {
    uint64_t x;
    uint32_t x_16_lo, x_32_mid_lo, x_8_mid_hi, x_4_hi_lo, x_4_hi_hi;
    bcset(&x_16_lo, bc_field_offset, 0x0000);
    bcset(&x_16_lo, bc_field_size, 0x02);
    bcset(&x_16_lo, bc_field_bitmask, 0xff);
    bcset(&x_32_mid_lo, bc_field_offset, 0x0002);
    bcset(&x_32_mid_lo, bc_field_size, 0x04);
    bcset(&x_32_mid_lo, bc_field_bitmask, 0xff);
    bcset(&x_8_mid_hi, bc_field_offset, 0x0006);
    bcset(&x_8_mid_hi, bc_field_size, 0x01);
    bcset(&x_8_mid_hi, bc_field_bitmask, 0xff);
    bcset(&x_4_hi_lo, bc_field_offset, 0x0007);
    bcset(&x_4_hi_lo, bc_field_size, 0x01);
    bcset(&x_4_hi_lo, bc_field_bitmask, 0x0f);
    bcset(&x_4_hi_hi, bc_field_offset, 0x0007);
    bcset(&x_4_hi_hi, bc_field_size, 0x01);
    bcset(&x_4_hi_hi, bc_field_bitmask, 0xf0);
    bcset(&x, x_16_lo, 0xd00d);
    bcset(&x, x_32_mid_lo, 0xdeadbeef);
    bcset(&x, x_8_mid_hi, 0x5a);
    bcset(&x, x_4_hi_lo, 0x5);
    bcset(&x, x_4_hi_hi, 0xa);
    printf("%016zx\n", x);
    printf("%01hhx\n", bcget(&x, x_4_hi_hi));
    printf("%01hhx\n", bcget(&x, x_4_hi_lo));
    printf("%02hhx\n", bcget(&x, x_8_mid_hi));
    printf("%08x\n", bcget(&x, x_32_mid_lo));
    printf("%04hx\n", bcget(&x, x_16_lo));
    return 0;
    /* expected output:
     * a55adeadbeefd00d
     * a
     * 5
     * 5a
     * deadbeef
     * d00d
     */
}
